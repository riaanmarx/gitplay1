:: find IP address in scriptable format
:: !!Windows 7 specific at the moment!!
:: Note this only works with one adapter connected
@echo off
:: get ipv4

c:

if not exist c:\files mkdir c:\files
if not exist c:\files\fixname mkdir c:\files\fixname

cd \files\fixname

ipconfig | findstr IPv4 > ipadd.txt

:: For statement to find the numbers
for /F "tokens=14" %%i in (ipadd.txt) do ( 
@echo %%i > iponly.txt
hostname > nameonly.txt
)

rem for /F "tokens=1" %%y in (iponly.txt) do ( 
rem echo %%y
rem nslookup %%y | findstr "Name:" > nslookupname.txt 
rem )

for /F "tokens=2 delims=:" %%x in (nslookupname.txt) do ( 
echo %%x > dnsnameF.txt
)

for /F "tokens=1 delims=." %%w in (dnsnameF.txt) do ( 
echo %%w > dnsname.txt
)

WMIC computersystem where caption='%computername%' rename new1

shutdown -t 2 -f -r 

rem del ipadd.txt /Q
pause